library('readxl')
comparison_table = read_excel('~/Documents/river_collab_comparison.xlsx')

library('ggplot2')
library('grid')
library('gridExtra')
colnames(comparison_table) = c('study_name', 'kappa', 'dice_ceoff', 'user_accuracy', 'producer_accuracy', 'training_area_sq_km', 'landform', 'data', 'notes')

user_accuracy <-ggplot(comparison_table, aes(x = training_area_sq_km, y = user_accuracy, color=study_name, shape=data)) + 
  geom_point() +
  scale_x_continuous(trans='log2') 

user_accuracy

producer_accuracy <-ggplot(comparison_table, aes(x = training_area_sq_km, y = producer_accuracy, color=study_name, shape=data)) + 
  geom_point() +
  scale_x_continuous(trans='log2') + theme(legend.position = "None")
user_accuracy & producer_accuracy

grid.newpage()
grid.arrange(ggplotGrob(user_accuracy), ggplotGrob(producer_accuracy), nrow=2, ncol = 1, widths=c(1,1))

grid.newpage()
grid.draw(rbind(ggplotGrob(user_accuracy), ggplotGrob(producer_accuracy), size='last'))
