# mlflow examples

this is a tech time mlflow example presentation given by Jonathan Garber.

Note that this is based of of the sklearn_elasticnet_wine and fastai examples from the mlflow/examples folder

To run the code first create an environment either via conda:

```
conda create -n mlflow_workshop -f mlflow_workshop.yml
```

and activate the environment:

```
conda activate mlflow_workshop
```

If not using conda, then use pip:

```
pip install -r requirements.txt
```

## Create Experiments

we need to create a couple experiments that the code submits to for the fastai and elasticnet examples, with your environment activated, and in the repository folder on a terminal/ command prompt, run the following:

```
mlflow experiments create -n fastai
```

then

```
mlflow experiments create -n sklearn

```

## Run the model several times

run both the fastai and sklearn model one or several times to populate your mlflow database:

```
python train_fastai.py
```

and

```
python train_sklearn.py
```

## Open the UI

mlflow has a super handy user interface that you can access to view your experiments and runs, Open it this way from the terminal within the package folder, and with the environment activated:

```
mlflow ui
```


## Accessing data via R

The nice thing about mlflow is that it has an R package for running models and seeing data, that can be run alongside the python package (Note, I couldn't get it to work with R installed in my conda environment, so install R somewhere else and make sure to use that). Check out the examples in `accessing_pipeline.Rmd` to see how to point R to the right python environment and mlflow executable, and access data in the backend store.

## Accessing data via Python

The same mlflow package that is logging data for each run, can also be used to access run data and models. See `results_viewer.ipynb` for examples.